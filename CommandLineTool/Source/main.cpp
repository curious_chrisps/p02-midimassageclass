//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include "midiMessageClass.h"
using namespace std;

int main()
{
    // insert code here...
    MidiMessage note;
    
    //--------------------------------------------- NoteNum Member functions
    note.setNoteNumber(-20);
    cout << endl << "Note Number is: " << note.getNoteNumber() << endl;
    cout << "Note in Hertz is: " << note.getMidiNoteInHertz() << endl;
    
    
    //--------------------------------------------- Velocity Member functions
    note.setVelocity(130);
    cout << endl << "Velocity is: " << note.getIntVelocity() << endl;
    cout << "Amplitude (Velocity between 0 and 1) is: " << note.getFloatVelocity() << endl;
    
    //--------------------------------------------- ChannelNumber Member functions
    note.setChannelNum(5000);
    cout << endl << "Channel number is: " << note.getChannelNum() << endl;
    
    
    MidiMessage note2(22, 33, 11);
    cout << endl << "note2 Note: " << note2.getNoteNumber() << endl;
    cout << "note2 Velo: " << note2.getIntVelocity() << endl;
    cout << "note2 FloatVelo: " << note2.getFloatVelocity() << endl;
    cout << "note2 Chan: " << note2.getChannelNum() << endl;


    
    cout << endl << "That's it.\n";
    return 0;
}
