//
//  midiMessageClass.h
//  CommandLineTool
//
//  Created by Christoph Schick on 30/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#pragma once

class MidiMessage
{
public:
    
    /** Default constructor */
    MidiMessage();   // Constructor
    
    /** Constructor with arguments */
    MidiMessage(int initialNumber, int initialVelocity, int initialChannel);    // Constructor with args
    
    /** Destructor */
    ~MidiMessage();  // Destructor
    
    
    /** Sets the MIDI note number of the message */
    void setNoteNumber(int value);  // Mutator
    
    /** Returns the MIDI note number of the message as integer */
    int getNoteNumber() const;      // Accessor
    
    /** Returns the MIDI note number of the message as floating point number*/
    float getMidiNoteInHertz() const;      // Accessor
    
    
    /** Sets the velocity number of the message */
    void setVelocity(int value);   // Mutator
    
    /** Returns the velocity number of the message*/
    int getIntVelocity() const;       // Accessor
    
    /** Returns the velocity of this message as float number between 0 and 1 */
    float getFloatVelocity() const;    // Accessor
    
    
    
    /** Sets the MIDI channel number of the message */
    void setChannelNum(int value);   // Mutator
    
    /** Returns the MIDI channel number of the message */
    int getChannelNum() const;       // Accessor
    
private:
    int number;
    int velocity;
    int channelNum;
};

