//
//  midiMessageClass.cpp
//  CommandLineTool
//
//  Created by Christoph Schick on 30/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include "midiMessageClass.h"
#include <cmath>

using namespace std;


MidiMessage::MidiMessage() : number(60), velocity(100), channelNum(1)
{
    cout << "Constructor " << endl;
}

MidiMessage::MidiMessage(int initialNumber, int initialVelocity, int initialChannel)
{
    number = initialNumber;
    velocity = initialVelocity;
    channelNum = initialChannel;
}

MidiMessage::~MidiMessage()
{
    cout << "Destructor " << endl;

}

//--------------------------------------------- NoteNumber Mutator and Accessors
void MidiMessage::setNoteNumber(int value)
{
    if (value >= 0 && value <= 127)
    {
        number = value;
    }else
    {
        number = 0;
    }
}

int MidiMessage::getNoteNumber() const
{
    return number;
}

float MidiMessage::getMidiNoteInHertz() const
{
    return 440 * pow(2, (number - 69) / 12.0);
}

//--------------------------------------------- Velocity Mutator and Accessors
void MidiMessage::setVelocity(int value)
{
    if (value >= 0 && value <= 127)
    {
        velocity = value;
    }else
    {
        velocity = 1;
    }
}

int MidiMessage::getIntVelocity() const
{
    return velocity;
}

float MidiMessage::getFloatVelocity() const
{
    return velocity*0.00787;
}

//--------------------------------------------- ChannelNum Mutator and Accessor
void MidiMessage::setChannelNum(int value)
{
    if (value >= 0 && value <= 127)
    {
        channelNum = value;
    }else
    {
        channelNum = 0;
    }
}

int MidiMessage::getChannelNum() const
{
    return channelNum;
}
